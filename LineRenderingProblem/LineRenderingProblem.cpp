// LineRenderingProblem.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>
#include <cstdio>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

// Typedefs
typedef unsigned char byte;
typedef struct FramebufferObject
{
	unsigned int m_iFBO;
	unsigned int m_iColourBuffer;
	unsigned int m_iDepthBuffer;
	unsigned int m_iWidth;
	unsigned int m_iHeight;
} FramebufferObject;

// Settings
static const int MAX_PATH = 512;
static const int g_iWindowWidth = 1000;
static const int g_iWindowHeight = 800;

// Forward declarations
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
int InitFramebuffer(FramebufferObject& p_rtFBO, unsigned int nWidth, unsigned int nHeight);
bool ScreenShot(GLFWwindow* window, byte* pBuffer, int iPitch, int iWidth, int iHeight);

void InitializeRendering(int width, int height)
{
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glViewport(0, 0, width, height);
	glScissor(0, 0, width, height);
	float a_fAspectY2X = (float)height / width;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1.0, 1.0, -a_fAspectY2X, a_fAspectY2X, -1.0, 1.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// OpenGL stuff
	glDisable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);
}

GLfloat angle = 0.0f;
void Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::mat4 transform = glm::rotate(glm::mat4(1.0f), angle, glm::vec3(1.0, 0.0, 0.0));

	glPushMatrix();
	glLoadMatrixf(&transform[0][0]);

	glDepthRange(0.05f, 1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-0.5f, -0.5f, 0.0f);
	glVertex3f(0.5f, -0.5f, 0.0f);
	glVertex3f(0.5f, 0.5f, 0.0f);
	glVertex3f(-0.5f, 0.5f, 0.0f);
	glEnd();

	glDepthRange(0.0f, 1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-0.5f, -0.5f, 0.0f);
	glVertex3f(0.5f, -0.5f, 0.0f);
	glVertex3f(0.5f, 0.5f, 0.0f);
	glVertex3f(-0.5f, 0.5f, 0.0f);
	glEnd();

	glPopMatrix();
}

int paused = 0;

int main()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_ANY_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	GLFWwindow* window = glfwCreateWindow(1000, 800, "LineRenderingProblem", nullptr, nullptr);
	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	//glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Failed to initialise GLEW" << std::endl;
		return -1;
	}

	const char* vendor = (const char*)glGetString(GL_VENDOR);
	if (vendor)
	{
		std::cout << "GL_VENDOR: " << vendor << std::endl;
	}

	const char* renderer = (const char*)glGetString(GL_RENDERER);
	if (renderer)
	{
		std::cout << "GL_RENDERER: " << renderer << std::endl;
	}

	if (!GLEW_VERSION_2_0)
	{
		std::cout << "OpenGL 2.0 not supported" << std::endl;
		return -1;
	}

	int width, height;
	glfwGetFramebufferSize(window, &width, &height);

	InitializeRendering(width, height);

	glfwSetKeyCallback(window, key_callback);

	GLfloat lastFrame = 0.0;

	while (!glfwWindowShouldClose(window))
	{
		if (paused)
			continue;

		// check and call events
		glfwPollEvents();

		// rendering
		GLfloat currentFrame = GLfloat(glfwGetTime());

		angle = glm::radians(90.0f * currentFrame);

		Render();

		// swap buffers
		glfwSwapBuffers(window);
	}

	glfwTerminate();

	return 0;
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
	if (key == GLFW_KEY_S && action == GLFW_PRESS)
	{
		// Capture screen shot
		paused = 1;
		unsigned int* pBuf = new unsigned int[g_iWindowWidth * g_iWindowHeight];
		ScreenShot(window, reinterpret_cast<byte*>(pBuf), g_iWindowWidth << 2, g_iWindowWidth, g_iWindowHeight);

		static int s_iScreenShot = 1;
		char szFileName[MAX_PATH];
		sprintf(szFileName, "ScreenShots/Capture%2d.bmp", s_iScreenShot++);
		stbi_write_bmp(szFileName, g_iWindowWidth, g_iWindowHeight, 4, reinterpret_cast<void*>(pBuf));
		delete[] pBuf;
		paused = 0;
	}
}

int InitFramebuffer(FramebufferObject& p_rtFBO, unsigned int nWidth, unsigned int nHeight)
{
	if (!p_rtFBO.m_iFBO || nWidth != p_rtFBO.m_iWidth || nHeight != p_rtFBO.m_iHeight )
	{
		if (p_rtFBO.m_iFBO || p_rtFBO.m_iColourBuffer)
		{
			// existing texture has the wrong dimensions, delete it
			glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, p_rtFBO.m_iFBO);
			glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, 0, 0);
			glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);
			glDeleteRenderbuffersEXT(1, &p_rtFBO.m_iDepthBuffer);
			glDeleteRenderbuffersEXT(1, &p_rtFBO.m_iColourBuffer);
			p_rtFBO.m_iDepthBuffer = 0;
			p_rtFBO.m_iColourBuffer = 0;
		}

		glEnable(GL_TEXTURE_2D);
		if (glActiveTexture)
			glActiveTexture(GL_TEXTURE0);
		else if (glActiveTextureARB)
			glActiveTextureARB(GL_TEXTURE0_ARB);

		if (!p_rtFBO.m_iFBO)
		{
			glGenFramebuffersEXT(1, &p_rtFBO.m_iFBO);
			glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, p_rtFBO.m_iFBO);
		}

		glGenRenderbuffersEXT(1, &p_rtFBO.m_iColourBuffer);
		glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, p_rtFBO.m_iColourBuffer);
		glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_RGBA, nWidth, nHeight);

		glGenRenderbuffersEXT(1, &p_rtFBO.m_iDepthBuffer);
		glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, p_rtFBO.m_iDepthBuffer);

		glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, nWidth, nHeight);

		glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_RENDERBUFFER_EXT, p_rtFBO.m_iColourBuffer);
		glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, p_rtFBO.m_iDepthBuffer);

		glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);

		GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
		if (status == GL_FRAMEBUFFER_COMPLETE_EXT)
		{
			p_rtFBO.m_iWidth = nWidth;
			p_rtFBO.m_iHeight = nHeight;
		}
		else
		{
			glDeleteFramebuffersEXT(1, &p_rtFBO.m_iFBO);
			glDeleteRenderbuffersEXT(1, &p_rtFBO.m_iColourBuffer);
			glDeleteRenderbuffersEXT(1, &p_rtFBO.m_iDepthBuffer);

			p_rtFBO.m_iFBO = 0;
			p_rtFBO.m_iColourBuffer = 0;
			p_rtFBO.m_iDepthBuffer = 0;

			glBindTexture(GL_TEXTURE_2D, 0);
			glDisable(GL_TEXTURE_2D);

			return 0;
		}
	}

	return 1;
}

FramebufferObject g_tFBOScreenShot;

bool ScreenShot(GLFWwindow* window, byte* pBuffer, int iPitch, int iWidth, int iHeight)
{
	glfwMakeContextCurrent(window);

	if (!GLEW_EXT_framebuffer_object)
		// fail
		return false;

	if (!InitFramebuffer(g_tFBOScreenShot, g_iWindowWidth, g_iWindowHeight))
		return false;

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, g_tFBOScreenShot.m_iFBO);

	glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT);
	glReadBuffer(GL_COLOR_ATTACHMENT0_EXT);

	//	Render here
	InitializeRendering(g_iWindowWidth, g_iWindowHeight);
	Render();

	//	Capture
	glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT);
	glReadBuffer(GL_COLOR_ATTACHMENT0_EXT);

	int iViewport[4];

	glGetIntegerv(GL_VIEWPORT, iViewport);

	int i;

	if (!iWidth)
		iWidth = iViewport[2];
	if (!iHeight)
		iHeight = iViewport[3];

	for (i = iViewport[1]; i < iHeight; i++)
		glReadPixels(iViewport[0], iViewport[3] - i - 1, iWidth, 1, GL_RGBA, GL_UNSIGNED_BYTE, &pBuffer[i * iPitch]);

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);

	return true;
}
